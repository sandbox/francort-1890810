<?php

/**
 * @file
 * Administrative page callbacks for menu module.
 */

/**
 * Formulario para establecer el access token y habilitar el watchdog.
 */
function chileatiende_settings_form($form, $form_state){
  $form['chileatiende_access_token'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#title' => 'Access Token',
    '#default_value' => variable_get('chileatiende_access_token', ''),
    '#description' => t('Token para acceder al service de ChileAtiende')
  );
  $form['chileatiende_watchdog'] = array(
    '#type' => 'checkbox',
    '#title' => 'Activar Watchdog',
    '#default_value' => variable_get('chileatiende_watchdog', 0),
    '#description' => t('Si esta activado, se registrará en el log de drupal cada vez que se conecta a ChileAtiende'),
  );

  return system_settings_form($form);
}
