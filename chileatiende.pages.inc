<?php

/**
 * @file
 * User page callbacks for the chileatiende module.
 */


/**
 * Menu callback para disponibilizar el acceso para la lista de fichas o servicios 
 */
function chileatiende_page_main(){
  $items = array(
    l(t('Sheets'), 'chileatiende/sheets'),
    l(t('Services'), 'chileatiende/services')
  );
  return theme('item_list', array('items' => $items));
}

/**
 * Genera el formulario de busqueda de un servicio
 */
function chileatiende_search_service_form($form, $form_state, $service_id){
  $form['service_id'] = array(
    '#type' => 'textfield',
    '#title' => 'service',
    '#default_value' => $service_id,
    '#description' => 'Ingrese el ID del service',
    '#size' => 10,
    '#element_validate' => array('chileatiende_search_validate_id')
  );

  $form['search'] = array(
    '#type' => 'submit',
    '#value' => t('Search')
  );
  return $form;
}

/**
 * Submit handler para redireccionar al servicio consultado
 * 
 * @see chileatiende_search_service_form()
 */
function chileatiende_search_service_form_submit($form, &$form_state){
  $redirect = 'chileatiende/services/' . $form_state['values']['service_id'];
  $form_state['redirect'] = $redirect;
}

/**
 * Validador que limpia los espacios del texto ingresado
 * 
 * @see chileatiende_search_service_form()
 * @see chileatiende_search_sheet_form()
 */
function chileatiende_search_validate_id($element, &$form_state, $form){
  $form_state['values'][$element['#name']] = trim($form_state['values'][$element['#name']]);
}

/**
 * Genera el formulario de busqueda de una ficha
 */
function chileatiende_search_sheet_form($form, $form_state, $sheet_id){
  $form['sheet_id'] = array(
    '#type' => 'textfield',
    '#title' => 'sheet',
    '#default_value' => $sheet_id,
    '#description' => 'Ingrese el ID de la sheet',
    '#size' => 10,
    '#element_validate' => array('chileatiende_search_validate_id')
  );

  $form['search'] = array(
    '#type' => 'submit',
    '#value' => t('Search')
  );
  return $form;
}

/**
 * Submit handler para redireccionar a la ficha consultada
 */
function chileatiende_search_sheet_form_submit($form, &$form_state){
  $redirect = 'chileatiende/sheets/' . $form_state['values']['sheet_id'];
  $form_state['redirect'] = $redirect;
}

/**
 * Menu callback para imprimir una ficha
 */
function chileatiende_sheet_page($sheet_id, $filters = array()){
  $form_sheet = drupal_get_form('chileatiende_search_sheet_form', $sheet_id);
  $output = drupal_render($form_sheet);
  $output .= chileatiende_output('sheet', $sheet_id);

  return $output;
}

/**
 * Menu callback para imprimir un servicio
 */
function chileatiende_service_page($service_id, $filter = array()){
  $form_service = drupal_get_form('chileatiende_search_service_form', $service_id);
  $output = drupal_render($form_service);

  $output .= chileatiende_output('service', $service_id);
  $sheets = chileatiende_get_sheets_by_service($service_id);
  if(isset($sheets['sheets']['items'])){
    $output .= '<h2>sheets del service</h2>';
    $rows = array();
    foreach($sheets['sheets']['items'] as $sheet){
      $rows[] = array(
        l($sheet['codigo'], 'chileatiende/sheets/' . $sheet['codigo']),
        $sheet['titulo'],
      );
    }
    $output .= theme('table', array('rows' => $rows));
  }
  return $output;
}

/**
 * Menu callback para imprimir la lista de fichas
 */
function chileatiende_sheets_page(){
  $output = '';
  $next = null;
  if(isset($_GET['next']) && $_GET['next'] != ''){
    $next = $_GET['next'];
  }
  $data = chileatiende_get_list('fichas', $next);
  if(is_array($data)){
    $rows = array();

    foreach($data['fichas']['items']['ficha'] as $sheet){
      $rows[] = array(
        l($sheet['codigo'], 'chileatiende/sheets/' . $sheet['codigo']),
        $sheet['servicio'],
        $sheet['titulo'],
        $sheet['permalink']
      );
    }

    $vars = array(
      'rows' => $rows,
      'header' => array('Código', 'service', 'Título', 'Permalink')
    );
    $output .= theme('table', $vars);
    if($data['fichas']['nextPageToken']){
      $query = array(
        'next' => $data['fichas']['nextPageToken'],
        'prev' => isset($_GET['next']) ? $_GET['next'] : '',
      );
      if(isset($_GET['prev'])){
        $query_prev = array(
          'next' => $_GET['prev'],
        );
        $output .= l(t('Previous'), 'chileatiende/sheets', array('query' => $query_prev));
      }
      $output .= l(t('Next'), 'chileatiende/sheets', array('query' => $query));
    }
    return $output;
  }
  return '';
}

/**
 * Menu callback para imprimir la lista de servicios
 */
function chileatiende_services_page(){
  $output = '';

  $data = chileatiende_get_list('servicios');

  $rows = array();
  foreach($data['servicios']['items']['servicio'] as $service){
    $rows[] = array(
      l($service['id'], 'chileatiende/services/' . $service['id']),
      $service['nombre'],
      $service['url'],
      $service['mision']
    );
  }

  $vars = array(
    'rows' => $rows,
    'header' => array('Código', 'service', 'Url', 'Misión')
  );
  $output .= theme('table', $vars);

  return $output;
}